module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
        colors: {
            'terminal-purple': '#2d0922',
            'command-green': '#7cfd38',
            'command-blue': '#669be2',
            'white': '#FFFFFF',
        },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
